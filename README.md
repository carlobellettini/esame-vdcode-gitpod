# Classroom reservation system

Obiettivo dell'esercizio è progettare e realizzare un insieme di classi atte a produrre un semplice programma Java che si occupi di gestire la prenotazione di un'aula ai tempi della COVID. 

- tutti gli studenti (identificati univocamente da nome e cognome) che vogliono assistere a una lezione in presenza devono prenotare il posto;
- gli studenti prenotati possono cancellare la propria prenotazione;
- l'occupazione dell'aula non può superare il 20% della capienza nominale;
- per i professori è disponibile un elenco ordinato alfabeticamente degli studenti prenotati, di cui potrà scegliere all'atto della creazione quanti vederne (noi li abbiamo creati in modo che i due professori dell'esempio ne vedano rispettivamente max 5 e 10);
- l'ufficio in carico della gestione delle aule vede invece in ogni momento la percentuale di occupazione.

Il sistema non permette la prenotazione doppia degli studenti e rifiuta le prenotazioni una volta raggiunto il limite massimo.

Vengono fornite due *Viste* del sistema:

- [`StudentView`](src/main/java/it/unimi/di/prog2/esame/view/StudentView.java): mette a disposizione un `TextField` per immettere il nome e cognome di uno studente e due bottoni per decidere se aggiungere o cancellare la prenotazione dello studente; mette anche a disposizione due metodi `fail` e `success` per modificare opportunamente il colore dlella vista;
- [`DisplayView`](src/main/java/it/unimi/di/prog2/esame/view/DisplayView.java): permette di visualizzare un elenco di righe e viene usata sia per mostrare l'elenco degli studenti ai professori, sia per mostrare l'occupazione ai gestori dell'aula; mette anche a disposizione un metodo `size` che restituisce la dimensione (in linee) dell'elenco decisa alla creazione; 

Viene fornita anche una prima versione della classe  [`Main`](src/main/java/it/unimi/di/prog2/esame/Main.java) che permette d'istanziare la parte statica delle viste, di una interfaccia [`Presenter`](src/main/java/it/unimi/di/prog2/esame/presenter/Presenter.java) e della classe  [`StudentPresenter`](src/main/java/it/unimi/di/prog2/esame/presenter/StudentPresenter.java) già predisposta ad ascoltare gli eventi dei bottoni della vista  [`StudentView`](src/main/java/it/unimi/di/prog2/esame/view/StudentView.java).

Completare il codice, in modo da realizzare un'organizzazione del sistema di tipo *Model-View-Presenter*.

**TUTTE LE CLASSI DATE POSSONO ESSERE DA VOI MODIFICATE (CANCELLATE, COMPLETATE) PER ADERIRE A VOSTRE IDEE DI PROGETTAZIONE**



Lanciando il programma (tramite il task `run` di gradle) si ottiene una interfaccia come quella nella figura sottostante, e provando a inserire e rimuovere uno studente vengono presentate delle stringhe autoesplicative sulla console.

![GUI](gui0.png)

Il programma deve in particolare fare in modo che:

- Aggiungendo un studente (non vuoto)... si colora di verde, si aggiornano gli elenchi e viene visualizza la percentuale
![INSERIMENTO CORRETTO](gui1.png)

- Se si prova a aggiungere un nome vuoto, non viene aggiunge e si colora di rosso.

![INSERIMENTO VUOTO](gui2.png)

- Se si prova a aggiungere uno studente già presente non cambiano gli elenchi e la percentuale, e si colora di rosso.

- Se si rimuove uno studente presente,  si colora di verde e vengono aggiornate le altre viste.

- Se aggiungendo un nuovo studente viene superata la soglia di occupazione massima 
(nel nostro esempio 20% di 30 vuol dire max 6 studenti), non viene aggiunto nulla e si colora di rosso.

![INSERIMENTO OLTRE CAPACTIA'](gui3.png)

  Da notare che, come appare nalle immagine, viene segnalato errore al tentativo di aggiunta del settimo studente (Paperon de Paperoni), e che il prof. Monga visualizza (coerentemente con sua inizializzazione) solo i primi 5 iscritti.


## SUGGERIMENTI

Per semplificare il riconoscimento di studenti già inseriti e il loro ordinamento, è possibile uniformarne la memorizzazione e presentazione in stringhe di soli caratteri maiuscoli.

## ALTRE RICHIESTE E DOMANDA

Vi abbiamo al momento fornito classi concrete (e l'interfaccia `Presenter`), qualora lo riteneste utile per un buon design potete chiaramente introdurre astrazioni (interfacce o classi astratte), corredando l'aggiunta con la spiegazione del perché i cambiamenti effettuati migliorino il progetto. Nel caso, invece, non riteneste opportuno l'uso di ulteriori interfacce e classi astratte, motivate questa scelta.

## CONSEGNA

Creare uno file `.zip` il contenuto della directory `src` e l'eventuale file contenente la risposta alla domanda.

Farne l'upload su il sito https://upload.di.unimi.it


