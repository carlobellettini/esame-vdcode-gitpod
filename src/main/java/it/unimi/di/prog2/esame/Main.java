package it.unimi.di.prog2.esame;


import it.unimi.di.prog2.esame.model.Model;
import it.unimi.di.prog2.esame.presenter.DisplayPresenter;
import it.unimi.di.prog2.esame.presenter.StudentPresenter;
import it.unimi.di.prog2.esame.view.DisplayView;
import it.unimi.di.prog2.esame.view.StudentView;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Main extends Application {
  final public static int COVID_RATIO = 20;
  final public static int OCCUPANCY_LAMBDA = 30;
  final public static int MAX_STUDENTS_PROF = 5;

  
  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) {

    primaryStage.setTitle("Classroom LAMBDA reservations");

    StudentView student = new StudentView("Student");

    DisplayView prof1 = new DisplayView(MAX_STUDENTS_PROF, "Monga:");
    DisplayView prof2 = new DisplayView(MAX_STUDENTS_PROF*2, "Bellettini:");


    GridPane gridPane = new GridPane();
    gridPane.setBackground(new Background(new BackgroundFill(Color.DARKOLIVEGREEN, CornerRadii.EMPTY, Insets.EMPTY)));
    gridPane.setPadding(new Insets(10, 10, 10, 10));

    gridPane.add(prof1, 0, 1);
    gridPane.add(prof2, 1, 1);


    gridPane.add(student, 0, 0);
    GridPane.setColumnSpan(student, GridPane.REMAINING);

    DisplayView caslod = new DisplayView(1, "Occupancy (max " + COVID_RATIO + "%):");
    gridPane.add(caslod, 0, 2);
    caslod.set(0, "0.00 %");
    GridPane.setColumnSpan(caslod, GridPane.REMAINING);


    Model model = new Model();
    new StudentPresenter(student, model);
    new DisplayPresenter(prof1, model);
    new DisplayPresenter(prof2, model);

    Scene scene = new Scene(gridPane);
    primaryStage.setScene(scene);
    primaryStage.show();

    //HINT: utile dopo aver definito model per inizializzare viste
    model.notifyObservers();
  }
}
