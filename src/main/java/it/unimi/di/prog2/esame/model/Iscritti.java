package it.unimi.di.prog2.esame.model;

import it.unimi.di.prog2.esame.Main;
import org.jetbrains.annotations.NotNull;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class Iscritti implements Iterable<Student> {
  private final Set<Student> composizione = new TreeSet<>();

  public boolean add(@NotNull Student stud) {
    if (isFull()) return false;
    return composizione.add(stud);
  }

  public boolean remove(@NotNull Student stud) {
    return composizione.remove(stud);
  }

  @Override
  public @NotNull Iterator<Student> iterator() {
    return composizione.iterator();
  }

  private boolean isFull() {
    return (composizione.size() + 1) * 100 > (Main.OCCUPANCY_LAMBDA * Main.COVID_RATIO);
  }
}
