package it.unimi.di.prog2.esame.model;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Model implements  Subject {

  private final List<Observer> observers = new ArrayList<>();
  private final Iscritti state = new Iscritti();

  public boolean addStudente(@NotNull Student studente) {
    if (state.add(studente)) {
      notifyObservers();
      return true;
    }
    return false;
  }

  public boolean removeStudente(@NotNull Student studente) {
    if (state.remove(studente)) {
      notifyObservers();
      return true;
    }
    return false;
  }

  public @NotNull Iterator<Student> iterator() {
    return state.iterator();
  }

  @Override
  public void addObserver(@NotNull Observer obs) {
    observers.add(obs);
  }

  @Override
  public void notifyObservers() {
    for (Observer observer : observers) {
      observer.update(this);
    }
  }

}
