package it.unimi.di.prog2.esame.model;

import org.jetbrains.annotations.NotNull;

public interface Observer {
  void update(@NotNull Subject subj);
}
