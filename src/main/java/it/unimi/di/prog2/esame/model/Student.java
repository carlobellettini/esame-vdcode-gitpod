package it.unimi.di.prog2.esame.model;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;

public class Student implements Comparable<Student> {
  final private String nome;

  public Student(@NotNull String nome) {
    this.nome = nome.toUpperCase(Locale.ROOT);
  }

  @Override
  public int compareTo(@NotNull Student o) {
    return nome.compareTo(o.nome);
  }

  @Override
  public String toString() {
    return nome;
  }
}
