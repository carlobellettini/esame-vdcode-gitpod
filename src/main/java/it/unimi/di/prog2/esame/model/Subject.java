package it.unimi.di.prog2.esame.model;

public interface Subject extends Iterable<Student> {
  void addObserver(Observer obs);

  void notifyObservers();
}
