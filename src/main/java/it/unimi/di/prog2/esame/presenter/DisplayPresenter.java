package it.unimi.di.prog2.esame.presenter;

import it.unimi.di.prog2.esame.model.Observer;
import it.unimi.di.prog2.esame.model.Student;
import it.unimi.di.prog2.esame.model.Subject;
import it.unimi.di.prog2.esame.view.DisplayView;
import org.jetbrains.annotations.NotNull;

public class DisplayPresenter implements Observer {
  private final DisplayView view;

  public DisplayPresenter(@NotNull DisplayView view, @NotNull Subject subj) {
    this.view = view;
    subj.addObserver(this);
  }

  @Override
  public void update(@NotNull Subject subj) {
    int pos = 0;
    for (Student student : subj) {
      if (pos < view.size())
        view.set(pos++, student.toString());
      else
        break;
    }
    while (pos < view.size()) {
      view.set(pos++, "");
    }
  }
}
