package it.unimi.di.prog2.esame.presenter;

public interface Presenter {
  void action(int id, String text);
}
