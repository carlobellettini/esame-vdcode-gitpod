package it.unimi.di.prog2.esame.presenter;


import it.unimi.di.prog2.esame.model.Model;
import it.unimi.di.prog2.esame.model.Student;
import it.unimi.di.prog2.esame.view.StudentView;
import org.jetbrains.annotations.NotNull;

public class StudentPresenter implements Presenter {

  private final StudentView view;
  private final Model model;

  public StudentPresenter(@NotNull final StudentView view, @NotNull Model model) {
    this.view = view;
    this.model = model;
    view.addHandlers(this);
  }

  public final void action(int cmd, String name) {
    //System.err.printf(cmd == StudentView.ADD ? "Adding %s\n" : "Removing %s\n", name);
    if (name == null || name.trim().equals("")) {
      view.fail();
      return;
    }
    if (cmd == StudentView.ADD && model.addStudente(new Student(name)) ||
        cmd != StudentView.ADD && model.removeStudente(new Student(name)))
      view.success();
    else
      view.fail();
  }
}
