package it.unimi.di.prog2.esame.view;

import it.unimi.di.prog2.esame.presenter.Presenter;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Pair;
import org.jetbrains.annotations.NotNull;

import java.util.AbstractMap.SimpleEntry;

public class StudentView extends Region {


  public static final int ADD = 0;
  private final Button[] buttons;
  private final Label title;
  private final TextField name;


  public StudentView(@NotNull String team) {
    buttons = new Button[2];
    name = new TextField();

    setBackground(new Background(
        new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(5.0), Insets.EMPTY)));
    setBorder(new Border(
        new BorderStroke(null, BorderStrokeStyle.SOLID, new CornerRadii(5.0), new BorderWidths(2))));

    GridPane grid = new GridPane();
    grid.setPadding(new Insets(10, 10, 10, 10));
    title = new Label(team);
    title.setFont(Font.font("sans", 20));
    grid.add(title, 0, 0);
    grid.add(name, 0, 1);
    GridPane.setColumnSpan(name, GridPane.REMAINING);
    GridPane.setColumnSpan(title, GridPane.REMAINING);
    for (int i = 0; i < 2; i++) {
      buttons[i] = new Button(i == ADD ? "Add" : "Remove");
      grid.add(buttons[i], i, 2);
    }

    this.getChildren().add(grid);
  }

  public void addHandlers(@NotNull Presenter presenter) {
    for (int i = 0; i < 2; i++) {
      final int id = i;
      buttons[i].setOnAction(eh -> presenter.action(id, name.getText()));
    }
    name.setOnAction(eh -> presenter.action(ADD, name.getText()));
  }

  public void success() {
    setBackground(new Background(
        new BackgroundFill(Color.LIGHTGREEN, new CornerRadii(5.0), Insets.EMPTY)));
  }

  public void fail() {
    setBackground(new Background(
        new BackgroundFill(Color.INDIANRED, new CornerRadii(5.0), Insets.EMPTY)));
  }
}
